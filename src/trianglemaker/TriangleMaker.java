package trianglemaker;

import java.util.Scanner;

/**
 *
 * @author alireza
 */
public class TriangleMaker
{
        public static Scanner scanner=new Scanner(System.in);//creates a scanner frome class scanner for reading from inpute buffer
        public static float a,b,c;//main variables
        public static void main(String[] args)
        {
                trianglemaker.tatal.argsReader(args);
        }
        /**
         * The main function of program that checks can we create a triangle with given edge sizes
         * @return boolean
         */
        public static boolean CanBeMade(float Edge_a ,float Edge_b, float Edge_c)
        {
                return (Edge_a+Edge_b>Edge_c)&&(Edge_a+Edge_c>Edge_b)&&(Edge_b+Edge_c>Edge_a);//a simple form of "if else" statement
        }
        public static void print(String string,boolean nextline)//just simplyfing printing
        {
                if (nextline)
                        System.out.print(string+"\n");
                if (!nextline)
                        System.out.print(string);
        }
        public static void CmdMod()//runs programm in commandline mode
        {
                print("Hello",true);
                print("Welcome to TM app",true);
                while (true)
                {                        
                        try//reads a value for edge a untile reciving a valide number
                        {
                                print("Please inter a number for edge a :",false);
                                a=Float.parseFloat(scanner.nextLine());
                                break;
                        }
                        catch (Exception e)
                        {
                                print("INVALID INPUT! Try again", true);
                        }
                }
                while (true)
                {                        
                        try//reads a value for edge b untile reciving a valide number
                        {
                                print("Please inter a number for edge b :",false);
                                b=Float.parseFloat(scanner.nextLine());
                                break;
                        }
                        catch (Exception e)
                        {
                                print("INVALID INPUT! Try again", true);
                        }   
                }
                while (true)
                {                        
                        try//reads a value for edge c untile reciving a valide number
                        {
                                print("Please inter a number for edge c :",false);
                                c=Float.parseFloat(scanner.nextLine());
                                break;
                        }
                        catch (Exception e)
                        {
                                print("INVALID INPUT! Try again", true);
                        }
                }
                /*
                checks if a triangel can be made by given edge sizes
                prints Yes if true and prints No if false
                */
                if(CanBeMade(a, b, c))
                        print("Yes", true);
                if(!CanBeMade(a, b, c))
                        print("No", true);
        }
        /**
         * just starts the program in gui mode
         * @return a gui form
         */
        public static void GuiMod()
        {
                trianglemaker.TMG tmg=new trianglemaker.TMG();
                tmg.setVisible(true);
        }
}
