package trianglemaker;
import java.util.Arrays;
/**
 *
 * @author alireza
 */
public class tatal
{
        /**
        * This method reads and processes the arguments given by user
        */
        public static void argsReader(String[] args)
        {
                if ((Arrays.binarySearch(args, "-a")> -1))
                        printAboutVersion(false, true);
                else if(Arrays.binarySearch(args, "-v")> -1)
                        printAboutVersion(true, false);
                else if(Arrays.binarySearch(args, "-h")> -1)
                        printhelp();
                else if(Arrays.binarySearch(args, "--about")> -1)
                        printAboutVersion(false, true);
                else if(Arrays.binarySearch(args, "--author")> -1)
                        printAboutVersion(false, true);
                else if(Arrays.binarySearch(args, "--help")> -1)
                        printhelp();
                else if(Arrays.binarySearch(args, "--version")> -1)
                        printAboutVersion(true, false);
                else if(Arrays.binarySearch(args, "-av")> -1)
                        printAboutVersion(true, true);
                else if(Arrays.binarySearch(args, "-va")> -1)
                        printAboutVersion(true, true);
                else if (Arrays.binarySearch(args, "-c")> -1)
                        TriangleMaker.CmdMod();
                else if (Arrays.binarySearch(args, "-g")> -1)
                        TriangleMaker.GuiMod();
                else if (Arrays.binarySearch(args, "--gui")> -1)
                        TriangleMaker.GuiMod();
                else if (Arrays.binarySearch(args, "--no-gui")> -1)
                        TriangleMaker.CmdMod();
                else if (Arrays.binarySearch(args, "-gui=enable")> -1)
                        TriangleMaker.GuiMod();
                else if (Arrays.binarySearch(args, "-gui=disable")< -1)
                        TriangleMaker.CmdMod();
                else 
                        System.out.println("Wrong option!\nPlease use --help or -h to see help.");
        }
        /**
         * Prints help page contents
         * @return String
         */
        public static void printhelp()
        {
                System.out.println("Usage: TriangleMaker.java [OPTION...]\r\nTriangleMaker.java Checkes if a triangle can be made or no.\r\nThe program works in both GUI mode and commandline mode.\r\n\r\n Helping options:\r\n  -v, --version            shows version of the program\r\n  -h, --help               shows this screen\r\n  -a, --about,--author     shows author informations\r\n\r\n Operation modifiers:\r\n  -g, --gui                     starts app in gui\r\n  -gui=enable                   starts app in gui\r\n  -gui=disable                  starts app in commandline\r\n  -c, --no-gui                  starts app in commandline\r\n  \r\n  *powerd by java*");
        }
        /**
         * Prints version and author informations
         * @return String
         */
        public static void printAboutVersion(boolean version,boolean about)
        {
                if (about)
                        System.out.println("Author : AliReza Mohseni\r\nEmail  : x.alirezamohseni@gmail.com\r\nStudent at University of Isfahan \r\ndepartemant of mathematics statistics and computer science.\r\nLecturer : Dr Rafiee");
                if (version)
                        System.out.println("Version : 0.0.1-rc");
        }
}
